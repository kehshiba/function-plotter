// func -> generate main plot
function mainPlotFunction() {
    const A = parseFloat(document.getElementById('paramA').value);
    const B = parseFloat(document.getElementById('paramB').value);
    const C = parseFloat(document.getElementById('paramC').value);
    const K = parseFloat(document.getElementById('paramK').value);
    const X = parseFloat(document.getElementById('paramX').value);
    const strokeWidth = parseFloat(document.getElementById('paramStroke').value);
    const curveSmoothingParam = document.getElementById('paramSmooth').checked ? d3.curveBasis : d3.curveLinear ;
    const strokeColor = document.getElementById('paramColor').value;
    const Y =  parseFloat(document.getElementById('paramY').value);

    //  margins and dimensions t
    const margin = { top: 50, right: 50, bottom: 50, left: 50 };
    const width = 1000 - margin.left - margin.right;
    const height = 400 - margin.top - margin.bottom;
    const range = d3.range(-X, X, 0.1);

    // remove any existing SVG (for dynamic changing)
    d3.select("svg").remove();

    
    // create svg
    const svg = d3.select("#graphingSpace")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .style("background", "white");

    //  main plot area inside SVG
    const mainPlotArea = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //  x-axis label
    mainPlotArea.append("text")
        .attr("class", "x label")
        .attr("text-anchor", "middle")
        .attr("x", width)
        .attr("y", height - 6)
        .text("x axis");

    //  y-axis label
    mainPlotArea.append("text")
        .attr("class", "y label")
        .attr("text-anchor", "middle")
        .attr("y", -50)
        .attr("x",-160)
        .attr("dy", ".90em")
        .attr("transform", "rotate(-90)")
        .text("y axis");

    // zoom behavior
    const handleZoom = (e) => mainPlotArea.attr('transform', e.transform);
    const zoom = d3.zoom()
                    .on('zoom', handleZoom)
                    .scaleExtent([1,10]);
    d3.select('svg').call(zoom);

    const xScale = d3.scaleLinear()
        .domain([-X, X])
        .range([0, width]);
    
    const yScale = d3.scaleLinear()
        .domain([-Y,Y])
        .range([height, 0]);
    
    // create x and y axes
    const xAxis = mainPlotArea.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + yScale(0) + ")")
        .call(d3.axisBottom(xScale));
    
    const yAxis = mainPlotArea.append("g")
        .attr("class", "axis")
        .call(d3.axisLeft(yScale));
    
    xAxis.selectAll("text")
        .style("text-anchor", "middle")
        .attr("dy", "1em")
        .attr("y", 10);
    
    yAxis.selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-0.5em")
        .attr("y", -5);
    
    // generate y values
    const yValues = range.map(
        x => {
            const y = Math.exp(-A * x) * Math.sin(B * K * Math.pow(x, C));
            return Math.max(-Y, Math.min(Y, y));
        }
    );
    
    // line generator
    const line = d3.line()
        .x((d, i) => xScale(range[i]))
        .y(d => yScale(d))
        .curve(curveSmoothingParam);
    
    // append path for main plot line
    mainPlotArea.append("path")
        .datum(yValues)
        .attr("fill", "none")
        .attr("stroke", strokeColor)
        .attr("stroke-width", strokeWidth)
        .attr("d", line);

    document.getElementById("zoomInButton").addEventListener("click", () => zoom.scaleBy(mainPlotArea, 1.2));
    document.getElementById("zoomOutButton").addEventListener("click", () => zoom.scaleBy(mainPlotArea, 0.8));
    document.getElementById("resetZoomButton").addEventListener("click", () => {
        mainPlotArea.transition().duration(500).call(zoom.transform, d3.zoomIdentity.translate(50,50).scale(1));
    });
}

// init func call
mainPlotFunction();

// func -> generate download link for SVG
function generateLink(fileName, url) {
    var link = document.createElement("a");
    link.download = fileName;
    link.href = url;
    document.body.appendChild(link); 
    return link;
}

// func -> save SVG
function saveSVG() {
    const svgElement = document.querySelector('#graphingSpace svg');
    const svgString = new XMLSerializer().serializeToString(svgElement);
    const blob = new Blob([svgString], { type: 'image/svg+xml;charset=utf-8' });
    const url = URL.createObjectURL(blob);

    const link = generateLink('plot.svg', url);
    link.click();
    URL.revokeObjectURL(url);
}

// func -> convert SVG to PNG
function SVG2PNG(svg, callback) {
    var canvas = document.createElement('canvas'); 
    var ctx = canvas.getContext('2d');
    var data = svg.outerHTML; 
    canvg(canvas, data); 
    callback(canvas); 
}

function updateValueSpan(inputId) {
    const input = document.getElementById(inputId);
    const valueSpan = document.getElementById(`${inputId}Value`);
    valueSpan.textContent = input.value;
}

//  event listeners to update value spans 
['paramA', 'paramB', 'paramC', 'paramK', 'paramX', 'paramY', 'paramStroke'].forEach(inputId => {
    const input = document.getElementById(inputId);
    input.addEventListener('input', () => {
        updateValueSpan(inputId);
    });
});

document.querySelectorAll('input[type="range"],input[type="color"], input[type="checkbox"]').forEach(input => {
    input.addEventListener('input', mainPlotFunction);
});

document.getElementById('saveSVGButton').addEventListener('click', saveSVG);

document.getElementById('savePNGButton').onclick = function() { 
    var element = document.querySelector('#graphingSpace svg'); 
    SVG2PNG(element, function(canvas) { 
        var base64 = canvas.toDataURL("image/png"); 
        generateLink('plot.png', base64).click(); 
    });
}
