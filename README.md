
## Function Plotter 

 
```
y(x) = e^(-Ax) * sin(B k x^C)
```
An interactive 2d function plotter developed using HTML and JavaScript. [View Demo](https://kehshiba.gitlab.io/function-plotter)

### External Libraries/Components used
- D3.js
- TailwindCSS 
- CanvG


### Functionalities

- 2D Graph to visualize function 
- Draggable and Zoom Controls
- Dynamic Controls for function parameters
- Adjust Stroke Width , Curve Smoothness and Stroke Color
- Export to SVG or PNG 


### Reasoning behind Libraries

#### D3.js
There are multiple charting/drawing libraries for plain javascript like chartjs or even function plotters developed using d3.js but the choice is due to a very simple fact, customizability and freedom over the library. D3 is very flexible compared to other libraries.


D3 renders as SVG which is resolution independent, thus helping make high-quality resolutions even on resizing, comparing with canvas based libraries.

Popular plotting libraries like Plotly is built on top of D3.js , thus using the base framework will give lots of customizability and can be integrated easily with other higher-level libraries when needed

#### CanvG
CanvG is used for converting the SVG graphics into PNG for export. There are multiple ways for SVG - PNG conversion like converting via [XML](https://arc.net/l/quote/cmyuidmp) but CanvG makes the process simpler , is very flexible when it comes to high resolution and widely supported in the community.

#### TailwindCSS
This was purely an aesthetic choice and for reducing dev time and all style can be easily replaced with plain CSS, and both plain CSS and TailwindCSS can be used together.



### Migration to Python

This architecture involves using JS ( or higher-level libraries) for client-side visualization and Python for the plotting backend.

For direct visualiztion from python, top choices would be d3py or vega-altair which gives visualization in SVG/PNG as well as XML/JSON to be served to client-side.

Using python for backend , we can optimize calculation speed with :
- Numpy Vectorization for faster calculation
- [JIT with Numba](https://numba.pydata.org/numba-doc/latest/user/jit.html)

Using a backend API calls for security considerations like Injection Attacks  - ```eval()``` as well as Server Side Attacks.

API can implement AES encryption for frontend-backend communication.

Users must have proper authorization and access tokens for their own plotting jobs.